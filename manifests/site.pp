package {'epel-release':
  ensure => 'installed',
  before => Class['collectd'],
}
$jointoken = hiera('token')
$home = hiera('home')
$cleanupdirs = hiera('cleanupdirs')
$api_server_cidr = hiera('api_server_cidr')
$splunk_forwarder_password = hiera('splunk_forwarder_password')
$splunk_forward_server = hiera('splunk_forward_server')
$deploymentclientconf = hiera('deploymentclientconf')

include docker
include collectd
include kubernetes
include selinux

node kubemaster {
  $kubestatus = $kubestate
  include facter
  include master
  Class['collectd'] -> Class['selinux'] -> Class['docker'] -> Class['kubernetes'] -> Class['facter'] -> Class['master']
}

node /^kubenode[0-9]+/ {
  include nodes
  Class['selinux'] -> Class['selinux'] -> Class['docker'] -> Class['kubernetes'] -> Class['nodes']
}
