# == Class: docker
#
# Full description of class docker here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { 'docker':
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2018 Your name here, unless otherwise noted.
#
class docker {
  package { yum-utils: ensure => installed }
  package { device-mapper-persistent-data: ensure => installed }
  package { lvm2: ensure => installed}
  
  yumrepo { 'docker-ce-stable':
    descr => 'Docker CE Stable - $basearch',
    baseurl => 'https://download.docker.com/linux/centos/7/$basearch/stable',
    enabled => 1,
    gpgcheck => 0,
  }

  package { 'docker-ce':
    ensure => installed,
    require => Yumrepo[ "docker-ce-stable" ],
  }
  service { 'docker':
    ensure => 'running',
    enable => true,
    require => Package[ "docker-ce" ],
  }
}
