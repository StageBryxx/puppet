class kubernetes::firewall{

  firewalld_port{'open port 8080 in the public zone':
    ensure => 'present',
    zone => 'public',
    port => 8080,
    protocol => 'tcp',
    require => Service['firewalld'],
    notify  => Systemctl['firewalld'],    
  }

  firewalld_port{'open port 10250 in the public zone':
    ensure => 'present',
    zone => 'public',
    port => 10250,
    protocol => 'tcp',
    require => Service['firewalld'],
    notify  => Systemctl['firewalld'],
  }

  firewalld_port{'open port 10255 in the public zone':
    ensure => 'present',
    zone => 'public',
    port => 10255,
    protocol => 'tcp',
    require => Service['firewalld'],
    notify  => Systemctl['firewalld'],
  }

  firewalld_service { 'Allow https':
    ensure  => 'present',
    service => 'https',
    zone    => 'public',
    require => Service['firewalld'],
    notify  => Systemctl['firewalld'],
  }

  sysctl { "net.bridge.bridge-nf-call-ip6tables":
    ensure => 'present',
    value  => 1,
  }

  sysctl { "net.bridge.bridge-nf-call-iptables":
    ensure => 'present',
    value => 1,
  }

  systemctl{'firewalld':
    command => 'restart',
  }

  service {'firewalld':
    ensure => 'running',
    enable => true,
  }
}
