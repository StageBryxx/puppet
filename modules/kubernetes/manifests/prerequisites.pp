class kubernetes::prerequisites{

  yumrepo { 'kubernetes-repo':
    descr   => 'kubernetes-repo',
    baseurl => 'https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64',
    enabled => 1,
    gpgcheck => 1,
    repo_gpgcheck => 1,
    gpgkey => 'https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg',
  }

  Package {
    ensure => installed,
    require => Yumrepo[ 'kubernetes-repo' ],
  }
  package { 'kubelet': }
  package { 'kubeadm': }
  package { 'kubectl': }

  file_line { 'fstab line':
    path  => '/etc/fstab',
    line  => '#\1',
    match => '^([^#].*swap.*)',
    notify => Exec[ 'turn off swap' ],
  }->

  exec { 'turn off swap':
    command => 'swapoff -a',
    provider => shell,
    refreshonly => true,
  }

  file_line { 'match cgroup driver':
    ensure             => present,
    path               => '/etc/systemd/system/kubelet.service.d/10-kubeadm.conf',
    line               => 'Environment="KUBELET_CGROUP_ARGS=--cgroup-driver=cgroupfs"',
    match              => '^Environment="KUBELET_CGROUP_ARGS',
    append_on_no_match => false,
    require => [ Package[ "kubelet", "kubeadm","kubectl"] ],
    notify => Exec[ 'reload daemon' ],
  }->

  exec {'reload daemon':
    command => 'systemctl daemon-reload',
    provider => shell,
    refreshonly => true,
  }

  systemctl {'kubelet':
    command => 'restart',
    require => Exec[ 'reload daemon' ],
  }
}
