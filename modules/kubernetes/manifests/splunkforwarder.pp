class kubernetes::splunkforwarder{

  file {"${home}/splunkforwarder-7.1.0-2e75b3406c5b-linux-2.6-x86_64.rpm":
    source => 'puppet:///modules/kubernetes/splunkforwarder-7.1.0-2e75b3406c5b-linux-2.6-x86_64.rpm',
    notify => Package[ 'splunkforwarder-7.1.0' ],
  }

  package {'splunkforwarder-7.1.0':
    ensure => 'present',
    source => "${home}/splunkforwarder-7.1.0-2e75b3406c5b-linux-2.6-x86_64.rpm",
    notify => Exec[ 'start splunk forwarder' ],
  }

  exec { 'start splunk forwarder':
    command => "/opt/splunkforwarder/bin/splunk start --accept-license --answer-yes --no-prompt --seed-passwd ${splunk_forwarder_password}",
    provider => shell,
    refreshonly => true,
    notify => Exec[ 'enable splunk forwarder' ],
  }

  exec { 'enable splunk forwarder':
    command => "/opt/splunkforwarder/bin/splunk enable boot-start",
    provider => shell,
    refreshonly => true,
    notify => Exec[ 'add forwarder server' ],
  }

  exec { 'add forward server':
    command => "/opt/splunkforwarder/bin/splunk add forward-server ${splunk_forward_server} -auth admin: ${splunk_forwarder_password}",
    provider => shell,
    refreshonly => true,
  }

  file { '/opt/splunkforwarder/etc/system/local/deploymentclient.conf':
  ensure  => file,
  content => "${deploymentclientconf}",
  notify => Exec[ 'restart splunk forwarder' ],
  }

  exec { 'restart splunk forwarder':
    command => "/opt/splunkforwarder/bin/splunk restart",
    provider => shell,
    refreshonly => true,
  }
}
