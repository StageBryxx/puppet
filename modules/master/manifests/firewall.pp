class master::firewall{
  notify {"status van de kube is ${kubestatus}":}

  firewalld_port{'open port 6443 in the public zone':
    ensure => 'present',
    zone => 'public',
    port => 6443,
    protocol => 'tcp',
    require => Service['firewalld'],
    notify  => Systemctl['firewalld']
  }

  firewalld_port{'open port 2379-2380 in the public zone':
    ensure => 'present',
    zone => 'public',
    port => '2379-2380',
    protocol => 'tcp',
    require => Service['firewalld'],
    notify  => Systemctl['firewalld']
  }

  firewalld_port{'open port 10250-10252 in the public zone':
    ensure => 'present',
    zone => 'public',
    port => '10250-10252',
    protocol => 'tcp',
    require => Service['firewalld'],
    notify  => Systemctl['firewalld']
  }
}
