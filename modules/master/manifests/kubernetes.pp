class master::kubernetes{

  file {'copy config file':
    path => "${home}/config.yaml",
    source => 'puppet:///modules/master/config.yaml',
  }->
  
  exec { 'init cluster':
    command => "kubeadm init --config ${home}/config.yaml",
    unless => "test ${kubestatus} == 'Ready'",
    provider => shell,
  }

  exec { 'create token':
    command => "kubeadm token create ${jointoken} --ttl 0",
    unless => "test ${kubestatus} == 'Ready'",
    provider => shell,
    require => Exec[ "init cluster" ],
  }

  #Dit kan misschien nog anders (met custom fact)
  file { 'create kube dir':
    path => "${home}/.kube",
    ensure => 'directory',
    recurse => true,
    require => Exec[ "init cluster" ],
  }

  file { 'copy conf file':
    path => "${home}/.kube/config",
    source => "file:///etc/kubernetes/admin.conf",
    owner     => 'root',
    group      => 'root',
    recurse => true,
    require => File[ "create kube dir" ],
  }

  exec { 'setup flannel network':
    command => "kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.9.1/Documentation/kube-flannel.yml",
    unless => "test ${kubestate} == 'Ready'",
    provider => shell,
    require => File[ "copy conf file"],
  }
}


### ANSIBLE CODE

#- name: Init Kubernetes cluster
#  shell: kubeadm init --apiserver-advertise-address {{ api_server_cidr }} --pod-network-cidr {{ flannel_network_cidr }} 

#- name: make kubectl work for the root user
#  shell: |
#    mkdir -p $HOME/.kube
#    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
#    sudo chown $(id -u):$(id -g) $HOME/.kube/config 

#- name: setup flannel network
  command: kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.9.1/Documentation/kube-flannel.yml

#- name: get join token
#  shell: kubeadm token create --print-join-command
#  register: join_token

#- name: create token fact
#  set_fact:
#    token: "{{ join_token.stdout }}"

#- name: echo token
#  shell: echo "{{ token }}


