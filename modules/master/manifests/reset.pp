class master::reset{
  exec { 'master kubeadm reset':
    command => 'kubeadm reset',
    provider => shell,
    unless => "test ${kubestatus} == 'Ready'",
    require => Package[ 'kubeadm' ],
    refresh => false,
  }

  exec { 'cleanup master folders':
    command =>  "rm -rf ${cleanupdirs}",
    provider => shell,
    unless => "test ${kubestatus} == 'Ready'",
    require => Exec[ 'master kubeadm reset' ],
    refresh => false,
  }
}
