class nodes::firewall{
  firewalld_port{'open port 30000-32767 in the public zone':
    ensure => 'present',
    zone => 'public',
    port => '30000-32768',
    protocol => 'tcp',
    require => Service['firewalld'],
    notify  => Systemctl['firewalld']
  }
}
