class nodes::kubernetes{
  exec { 'join cluster':
    command => "kubeadm join ${api_server_cidr}:6443 --token ${jointoken} --discovery-token-unsafe-skip-ca-verification",
    provider => shell,
    require => [Exec[ 'cleanup node folders' ], Package['kubeadm']],
  }
}


### ANSIBLE CODE

#---
#- name: run join command
#  shell: "{{ hostvars['master']['token'] }}"
