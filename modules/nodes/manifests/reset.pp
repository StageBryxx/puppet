class nodes::reset{
  exec { 'node kubeadm reset':
    command => 'kubeadm reset',
    provider => shell,
    require => Package[ 'kubeadm' ],
  }

  exec { 'cleanup node folders':
    command =>  "rm -rf ${cleanupdirs}",
    provider => shell,
    require => Exec[ 'node kubeadm reset' ],
  }
}
